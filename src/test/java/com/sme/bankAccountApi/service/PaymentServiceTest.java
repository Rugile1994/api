package com.sme.bankAccountApi.service;

import com.sme.bankAccountApi.dto.AccountResponse;
import com.sme.bankAccountApi.dto.PaymentRequest;
import com.sme.bankAccountApi.exception.BadRequestException;
import com.sme.bankAccountApi.model.Account;
import com.sme.bankAccountApi.repository.AccountRepository;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SpringBootTest
@AutoConfigureMockMvc
class PaymentServiceTest {
    private final AccountRepository accountRepository = mock(AccountRepository.class);
    private final PaymentService paymentService = new PaymentService(accountRepository);

    @ParameterizedTest
    @CsvSource({
            "1000236, 978, 1000.00, OPEN, 100.00, debit",
            "1000236, 978, 100.00, OPEN, 100.00, debit"
    })
    public void testDebit(int accountNumber, int currency, BigDecimal balance, String status, BigDecimal amount, String operation) {

        when(accountRepository.findByAccountNumber(accountNumber))
                .thenReturn(Optional.of(new com.sme.bankAccountApi.model.Account(accountNumber, currency, balance, status)));

        AccountResponse accountResponseAfterDebit = paymentService.postPayment(new PaymentRequest(accountNumber, amount, currency, operation));

        assertAll("Verify account details",
                () -> assertEquals(accountResponseAfterDebit.getAccountNumber(), accountNumber),
                () -> assertEquals(accountResponseAfterDebit.getCurrency(), currency),
                () -> assertEquals(accountResponseAfterDebit.getBalance(), balance.subtract(amount)),
                () -> assertEquals(accountResponseAfterDebit.getStatus(), status));
    }

    @ParameterizedTest
    @CsvSource({
            "1000236, 978, 1000.00, OPEN, 100.00, credit"
    })
    public void testValidCredit(int accountNumber, int currency, BigDecimal balance, String status, BigDecimal amount, String operation) {

        when(accountRepository.findByAccountNumber(accountNumber))
                .thenReturn(Optional.of(new com.sme.bankAccountApi.model.Account(accountNumber, currency, balance, status)));

        AccountResponse accountResponseAfterDebit = paymentService.postPayment(new PaymentRequest(accountNumber, amount, currency, operation));

        assertAll("Verify account details",
                () -> assertEquals(accountResponseAfterDebit.getAccountNumber(), accountNumber),
                () -> assertEquals(accountResponseAfterDebit.getCurrency(), currency),
                () -> assertEquals(accountResponseAfterDebit.getBalance(), balance.add(amount)),
                () -> assertEquals(accountResponseAfterDebit.getStatus(), status));
    }

    @ParameterizedTest
    @CsvSource({
            "1000236, 978, 1000.00, CLOSED, 978, 100.00, credit, Account 1000236 is closed!",
            "1000236, 974, 100.00, OPEN, 111, 100.00, debit, Wrong currency: 111",
            "1000236, 974, 1000.00, OPEN, 974, -100.00, debit, Amount is invalid: -100.00",
            "1000236, 974, 1000.00, OPEN, 974, 100.00, debfit, Payment operation name is invalid: debfit",
            "1000236, 974, 1000.00, OPEN, 974, 10000.00, debit, Balance is not enough!"
    })
    public void testPaymentWithError(int accountNumber, int currency, BigDecimal balance, String status, int operationCurrency, BigDecimal amount, String operation, String error) {

        when(accountRepository.findByAccountNumber(accountNumber))
                .thenReturn(Optional.of(new Account(accountNumber, currency, balance, status)));
        BadRequestException ex = assertThrows(BadRequestException.class, () ->
                paymentService.postPayment(new PaymentRequest(accountNumber, amount, operationCurrency, operation)));

        assertEquals(ex.getMessage(), error);
    }
}