package com.sme.bankAccountApi.service;


import com.sme.bankAccountApi.dto.AccountResponse;
import com.sme.bankAccountApi.repository.AccountRepository;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SpringBootTest
@AutoConfigureMockMvc
class AccountServiceTest {
    private final AccountRepository accountRepository = mock(AccountRepository.class);
    private final AccountService accountService = new AccountService(accountRepository);

    @Test
    public void testGetAccountStatus() {
        int accountNumber = 1000123;
        int currency = 978;
        BigDecimal balance = BigDecimal.valueOf(100.00);
        String status = "OPEN";

        when(accountRepository.findByAccountNumber(accountNumber)).thenReturn(Optional.of(new com.sme.bankAccountApi.model.Account(accountNumber, currency, balance, status)));

        assertEquals(accountService.getAccountStatus(accountNumber), status);
    }

    @Test
    public void testGetAccountBalance() {
        int accountNumber = 1000123;
        int currency = 978;
        BigDecimal balance = BigDecimal.valueOf(0);
        String status = "OPEN";

        when(accountRepository.findByAccountNumber(accountNumber))
                .thenReturn(Optional.of(new com.sme.bankAccountApi.model.Account(accountNumber, currency, balance, status)));

        AccountResponse accountResponse = accountService.getAccountBalance(accountNumber);
        assertAll("Verify account details",
                () -> assertEquals(accountResponse.getAccountNumber(), accountNumber),
                () -> assertEquals(accountResponse.getCurrency(), currency),
                () -> assertEquals(accountResponse.getBalance(), balance),
                () -> assertEquals(accountResponse.getStatus(), status));
    }

}