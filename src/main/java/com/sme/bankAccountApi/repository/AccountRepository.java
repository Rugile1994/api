package com.sme.bankAccountApi.repository;

import com.sme.bankAccountApi.model.Account;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface AccountRepository extends CrudRepository<Account, Long> {
    Optional<Account> findByAccountNumber(int accountNumber);
}
