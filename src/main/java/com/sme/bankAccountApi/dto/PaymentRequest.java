package com.sme.bankAccountApi.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.math.BigDecimal;

@Getter
@AllArgsConstructor
public class PaymentRequest {
    private int accountNumber;
    private BigDecimal amount;
    private int currency;
    private String operation;
}