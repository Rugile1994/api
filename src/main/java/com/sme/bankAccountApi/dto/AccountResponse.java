package com.sme.bankAccountApi.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.math.BigDecimal;

@Getter
@AllArgsConstructor
public class AccountResponse {
    private int accountNumber;
    private int currency;
    private BigDecimal balance;
    private String status;
}
