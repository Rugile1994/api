package com.sme.bankAccountApi.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "account")
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "account_number", nullable = false)
    private int accountNumber;

    @Column(name = "currency", nullable = false)
    private int currency;

    @Column(name = "balance")
    private BigDecimal balance;

    @Column(name = "status")
    private String status;

    public void debitAccount(BigDecimal amount) {
        this.setBalance(this.getBalance().subtract(amount));
    }

    public void creditAccount(BigDecimal amount) {
        this.setBalance(this.getBalance().add(amount));
    }

}
