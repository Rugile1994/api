package com.sme.bankAccountApi.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class BusinessError {
    private String description;
    private Date timestamp;

    public BusinessError(String description, Date timestamp) {
        this.description = description;
        this.timestamp = timestamp;
    }
}
