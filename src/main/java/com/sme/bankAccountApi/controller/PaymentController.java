package com.sme.bankAccountApi.controller;

import com.sme.bankAccountApi.dto.AccountResponse;
import com.sme.bankAccountApi.dto.PaymentRequest;
import com.sme.bankAccountApi.service.PaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/api/v1")
public class PaymentController {

    private final PaymentService paymentService;

    public PaymentController(PaymentService paymentService) {
        this.paymentService = paymentService;
    }

    @PostMapping(value = "/payment")
    public ResponseEntity<AccountResponse> postAccountDebt(@RequestBody PaymentRequest paymentRequest) {
        return ResponseEntity.status(HttpStatus.OK)
                .body(paymentService.postPayment(paymentRequest));
    }
}