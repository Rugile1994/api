package com.sme.bankAccountApi.controller;

import com.sme.bankAccountApi.dto.AccountResponse;
import com.sme.bankAccountApi.service.AccountService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/api/v1")
public class AccountController {

    private final AccountService accountService;

    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping("/accounts/{accountNumber}/status")
    public ResponseEntity<String> getAccountStatus(@PathVariable int accountNumber) {
        return ResponseEntity.status(HttpStatus.OK)
                .body(accountService.getAccountStatus(accountNumber));
    }

    @GetMapping("/accounts/{accountNumber}/balance")
    public ResponseEntity<AccountResponse> getAccountBalance(@PathVariable int accountNumber) {
        return ResponseEntity.status(HttpStatus.OK)
                .body(accountService.getAccountBalance(accountNumber));
    }

}
