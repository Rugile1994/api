package com.sme.bankAccountApi.service;

import com.sme.bankAccountApi.dto.AccountResponse;
import com.sme.bankAccountApi.exception.NotFoundException;
import com.sme.bankAccountApi.model.Account;
import com.sme.bankAccountApi.repository.AccountRepository;
import org.springframework.stereotype.Service;

@Service
public class AccountService {

    private final AccountRepository accountRepository;

    public AccountService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public String getAccountStatus(int accountNumber) {
        return accountRepository.findByAccountNumber(accountNumber)
                .orElseThrow(() -> new NotFoundException("Account number doesn't exist")).getStatus();
    }

    public AccountResponse getAccountBalance(int accountNumber) {
        Account account = accountRepository.findByAccountNumber(accountNumber)
                .orElseThrow(() -> new NotFoundException("Account number doesn't exist"));
        return new AccountResponse(account.getAccountNumber(), account.getCurrency(), account.getBalance(), account.getStatus());
    }
}
