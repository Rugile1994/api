package com.sme.bankAccountApi.service;

import com.sme.bankAccountApi.dto.AccountResponse;
import com.sme.bankAccountApi.dto.PaymentRequest;
import com.sme.bankAccountApi.exception.BadRequestException;
import com.sme.bankAccountApi.exception.NotFoundException;
import com.sme.bankAccountApi.model.Account;
import com.sme.bankAccountApi.repository.AccountRepository;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Objects;

@Service
public class PaymentService {

    private final AccountRepository accountRepository;

    public PaymentService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public AccountResponse postPayment(PaymentRequest paymentRequest) {
        Account account = accountRepository.findByAccountNumber(paymentRequest.getAccountNumber())
                .orElseThrow(() -> new NotFoundException("Account number doesn't exist: " + paymentRequest.getAccountNumber()));

        if (isPaymentValid(account, paymentRequest)) {
            makePayment(account, paymentRequest);
            accountRepository.save(account);
        }

        return new AccountResponse(account.getAccountNumber(), account.getCurrency(), account.getBalance(), account.getStatus());
    }

    private boolean isPaymentValid(com.sme.bankAccountApi.model.Account account, PaymentRequest paymentRequest) {
        if (!isAccountOpen(account.getStatus())) {
            throw new BadRequestException("Account " + account.getAccountNumber() + " is closed!");
        }
        if (!isCurrencyValid(account.getCurrency(), paymentRequest.getCurrency())) {
            throw new BadRequestException("Wrong currency: " + paymentRequest.getCurrency());
        }
        if (!isAmountValid(paymentRequest.getAmount())) {
            throw new BadRequestException("Amount is invalid: " + paymentRequest.getAmount());
        }
        if (Objects.equals("debit", paymentRequest.getOperation()) && !isBalanceEnough(account.getBalance(), paymentRequest.getAmount())) {
            throw new BadRequestException("Balance is not enough!");
        }
        return true;
    }

    private void makePayment(com.sme.bankAccountApi.model.Account account, PaymentRequest paymentRequest) {
        if (Objects.equals("debit", paymentRequest.getOperation())) {
            account.debitAccount(paymentRequest.getAmount());
        } else if (Objects.equals("credit", paymentRequest.getOperation())) {
            account.creditAccount(paymentRequest.getAmount());
        } else {
            throw new BadRequestException("Payment operation name is invalid: " + paymentRequest.getOperation());
        }
    }

    private boolean isAccountOpen(String status) {
        return Objects.equals("OPEN", status);
    }

    private boolean isCurrencyValid(int accountCurrency, int requestedCurrency) {
        return (accountCurrency == requestedCurrency);
    }

    private boolean isBalanceEnough(BigDecimal balance, BigDecimal amount) {
        return balance.compareTo(amount) >= 0;
    }

    private boolean isAmountValid(BigDecimal amount) {
        return amount.compareTo(BigDecimal.valueOf(0.00)) > 0;
    }
}
