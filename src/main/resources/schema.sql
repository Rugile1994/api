create table if not exists account (
    account_number int not null primary key,
    currency int not null,
    balance decimal,
    status varchar not null
);